import json


class Convert:
    @staticmethod
    def to_json(student):
        json_obj = json.dumps(student, indent=4)
        return json_obj
