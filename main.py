from script.core.student import Convert
obj = Convert()
name = input("Student name: ")
clg = input("College name: ")
dept = input("Department: ")
yr = input("Class")
student = {"Name": name, "College": clg, "Department": dept, "year": yr}
course_num = int(input("No. of courses: "))
courses = []
for i in range(course_num):
    course_name = input("Course Name: ")
    course_code = input("Course code: ")
    courses.append({"Course_name": course_name, "Course_code": course_code})
student["course_list"] = courses
print(obj.to_json(student))
